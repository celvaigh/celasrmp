import gzip,math
import matplotlib
matplotlib.use('Agg')
import pylab as plt
import numpy as np
import wikipedia
import os,sys,time
import pandas as pd
import scipy.sparse
import tqdm

from helper import *
class WSRM():
    def __init__(self, path,ontolgypath=None,plotname=None,output="output.npy",verbose=0):
        self.path=path
        self.verbose=verbose
        self.output=output
        self.plotname=plotname
        self.dic={}
        self.entities={}
        self.ontolgypath=ontolgypath
        self.name="Metric"
    def compute(self):
        t0=time.time()
        if os.path.isfile(self.path):files_names =[self.path]
        else:files_names = [self.path+f for f in os.listdir(self.path)]
        if self.verbose:print("starting ...")
        
        facts=0
        if "yagoFacts" in self.path:
            yids2num=np.load("data/Yago.mid2numsdic.npy",encoding="latin1",allow_pickle=True).item()
        elif "yagoSchemaSaturated" in self.path:
            yids2num=np.load("data/YagoSaturated.mid2numsdic.npy",encoding="latin1",allow_pickle=True).item()
        else:
            is_big=True
            yids2num=np.load("data/BaseKB.mid2numsdic.npy",allow_pickle=True).item()
            
        for filename in tqdm.tqdm(files_names):
            if self.verbose:
                print("procedding file :",filename)
                sys.stdout.flush()
            if ".gz" in filename:
                if "links-m-" not in filename:continue
                with gzip.open(filename,"rb") as f:
                    for i in f:
                        try:s,r,o,_=i.split()
                        except:continue
                        s,o=s.rsplit("/")[-1][:-1],o.rsplit("/")[-1][:-1]
                        try:self.dic[(s,o)]+=1
                        except:self.dic[(s,o)]=1
                        try:self.dic[(s,o)]+=subP[r]
                        except:pass
                        try:self.entities[s]+=1
                        except:self.entities[s]=1
                        try:self.entities[o]+=1
                        except:self.entities[o]=1
            else:
                with open(filename) as f: 
                    for i in tqdm.tqdm(f):
                        try:_,s,r,o,_=i.split("\t");facts+=1
                        except:continue
                        
                        if "rdf:type" in r:continue
                        if "/" in s:s=s.rsplit("/")[-1]
                        if "/" in o:o=o.rsplit("/")[-1]
                        for c in ["<",">","ressource:"]:s,o=s.replace(c,""),o.replace(c,"")
                        try:sid,oid=yids2num[s],yids2num[o]
                        except:continue
                        
                        try:self.dic[(sid,oid)]+=1
                        except:self.dic[(sid,oid)]=1
                        try:self.entities[sid]+=1
                        except:self.entities[sid]=1
                        try:self.entities[oid]+=1
                        except:self.entities[oid]=1
        
        wsrm={}
        if self.verbose:print("commonness : ",len(self.dic))
        if self.verbose:print("entities : ",len(self.entities))
        if self.verbose:print("facts : ",facts)
        for i in self.dic:wsrm[i]=float(self.dic[i])/self.entities[i[0]]
        print("toooook : ",time.time()-t0)
        np.save(self.output,wsrm)
    def plot(self):
            width=1.0
            if self.verbose:print("ploting distribution of links between entity pairs...")
            #plt.title('Distribution of links between entity pairs in BaseKB')
            plt.xlabel('Entity pairs links count')
            plt.ylabel('Occurrence')
            plt.hist(self.dic.values(),color="g")
            #plt.gca().set_xscale("log")
            plt.gca().set_yscale("log")
            plt.savefig(self.plotname)
            if self.verbose:print("#### End ###")

def REF(filename,ontology,is_big=False):
    if ontology=="yago":
        yids2num=np.load("data/Yago.mid2numsdic.npy",encoding="latin1",allow_pickle=True).item()
    elif ontology=="yagoS":
        yids2num=np.load("data/YagoSaturated.mid2numsdic.npy",encoding="latin1",allow_pickle=True).item()
    else:
        is_big=True
        yids2num=np.load("data/BaseKB.mid2numsdic.npy",allow_pickle=True).item()
    if is_big:data=open(filename)
    else:
        with open(filename) as f: data=f.read().split("\n")
    ref={}
    for i in tqdm.tqdm(data):
        try:_,s,r,o,_=i.split("\t")
        except:continue
        if "rdf:type" in r:continue
        if "/" in s:s=s.rsplit("/")[-1]
        if "/" in o:o=o.rsplit("/")[-1]
        for c in ["<",">","ressource:"]:s,o=s.replace(c,""),o.replace(c,"")
        try:sid,oid=yids2num[s],yids2num[o]
        except:continue
        ref[(sid,oid)]=1

    print(list(ref.keys())[:10])
    print(list(ref.values())[:10])
    print(len(ref))
    np.save("Dicts/{}.ref.npy".format(ontology),ref)

def toglove(entity2id,entity2vec):
    """
     Convert transe output to glove like embedding which can be used as entity relatedness for the linking.
    """
    e2i={}
    with open(entity2id) as f:data=f.read().split("\n")
    for i in tqdm.tqdm(data):
        try:idx,x=i.split("\t")
        except:continue 
        e2i[int(idx)]=x
    idx=0
    embeddings={}
    yid2wid=np.load("data/yid2wid.npy").item()
    with open(entity2vec) as f:data=f.read().split("\n")
    for vec in tqdm.tqdm(data):
        
        vec=vec.split()
        if len(vec)==100:
            try:
                yid=(e2i[idx].replace("<","")).replace(">","")
                embeddings[yid2wid[yid]]=np.array(map(float,vec))
            except:pass
        else:print(vec)
        idx+=1
    print("entities found : {}".format(len(embeddings)))
    np.save("data/transe_yago.npy",embeddings)


def process(filename):
    """
    Convert triples in KB to index used to train transe :
        https://github.com/thunlp/Fast-TransX/blob/master/transE/transE.cpp
    """
    data=[]
    if os.path.isfile(filename):
        for triple in tqdm.tqdm(open(filename)):
            try:s,r,o,_=triple.split("\t")
            except:pass
            if "type" in r:continue
            data+=[triple]
    else:
        
        for file in tqdm.tqdm(glob.glob(filename+"/links-m*")):
            for triple in gzip.open(file,"rb"):
                try:
                    s,r,o,_=triple.split("\t")
                    if "http://rdf.basekb.com/ns/m." in s and "http://rdf.basekb.com/ns/m." in o:data+=[triple]
                except:pass
    print("triples found {}".format(len(data)))
    with open("data.triples","w") as f:
        f.write("\n".join(data))
    
    ds,dr={},{}
    train=[]
    for line in tqdm.tqdm(data):
        try:s,r,o=line.split("\t")
        except:
            continue#print(line.split(","))
        o=" ".join(o)
        try:tmp=ds[s]
        except:ds[s]=len(ds)
        try:tmp=dr[r]
        except:dr[r]=len(dr)
        try:tmp=ds[o]
        except:ds[o]=len(ds)
        train+=[" ".join(map(str,[ds[s],ds[o],dr[r]]))]
        
    entities=[str(ds[i])+"\t"+i for i in ds]
    relations=[str(dr[i])+"\t"+i for i in dr]
        
    with open("relation2id.txt","w") as f:f.write("\n".join(map(str,[len(dr)]+relations)))
    with open("entity2id.txt","w") as f:f.write("\n".join(map(str,[len(ds)]+entities)))
    with open("train2id.txt","w") as f:f.write("\n".join([str(len(train))]+train))


def wsrm(filename,_plotname,_output):
    if "yagoFacts" in filename:
        onto = "Yago"
    elif "yagoSchemaSaturated" in filename:
        onto = "YagoSaturated"
    else:
        onto = "BaseKB"
    if not os.path.isfile("data/{}.mid2numsdic.npy".format(onto)):
        triples2ids(filename)
    kb=WSRM(filename,ontolgypath="",plotname=_plotname,output=_output,verbose=1)
    # kb.compute()
    dic2csv(_output)
    #kb.plot()

if __name__ == "__main__":
    wsrm("data/yagoFacts.tsv","","data/Yago.unarc.npy")
