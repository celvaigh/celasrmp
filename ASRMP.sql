#install page : https://developer.fedoraproject.org/tech/database/postgresql/about.html
http://climber2002.github.io/blog/2015/02/07/install-and-configure-postgresql-on-ubuntu-14-dot-04/
https://dba.stackexchange.com/questions/28926/moving-postgresql-data-to-different-drive
#sudo apt update
#sudo apt install postgresql postgresql-contrib
#\l:show databases
#\dt show tables
sudo su - postgres
createdb wsrm;
psql wsrm;

\timing on
################### Yago #########################

create table unarc(s int, o int, m float);
create index so on unarc (s,o);
create index os on unarc (o,s);
cluster unarc using so;
vacuum analyze;
copy unarc from 'Yago.unarc.csv' DELIMITER ',' CSV ;

create table candidateentities(e int);
create index pkce on candidateentities(e);
cluster candidateentities using pkce;
copy candidateentities from 'tac.yago.cands.tsv' DELIMITER ',' CSV ;

create table oneedgeonecandidateentity as select s,o,m from unarc where s in (select e from candidateentities) or o in (select e from candidateentities);
create index sooeoce on oneedgeonecandidateentity (s,o);
create index osoeoce on oneedgeonecandidateentity (o,s);
cluster oneedgeonecandidateentity using sooeoce;
vacuum analyze;

#Two edgepaths
   create table twoedgespaths as select e1.s,e2.o,e1.m as m1,e2.m as m2 from unarc as e1, unarc as e2 where e1.o=e2.s and e1.s!=e2.o and e1.s in (select e from candidateentities) and e2.o in (select e from candidateentities);
   

#Three edgepaths
   create table threeedgespaths as select e1.s, e3.o, e1.m as m1, e2.m as m2, e3.m as m3 from oneedgeonecandidateentity as e1, unarc as e2, oneedgeonecandidateentity as e3 where e1.o=e2.s and e1.s in (select e from candidateentities) and e2.o=e3.s and e3.o in (select e from candidateentities) and e1.s!=e3.o;
   

#Four edgepaths
   create table fouredgespaths as select e1.s, e4.o, e1.m as m1, e2.m as m2, e3.m as m3, e4.m as m4 from oneedgeonecandidateentity as e1, unarc as e2, unarc as e3, oneedgeonecandidateentity as e4 where e1.o=e2.s and e1.s in (select e from candidateentities) and e2.o=e3.s and e3.o=e4.s and e4.o in (select e from candidateentities) and e1.s!=e4.o;
   

################### YagoS #########################
create table unarc(s int, o int, m float);
create index so on unarc (s,o);
create index os on unarc (o,s);
cluster unarc using so;
vacuum analyze;
copy unarc from 'yagos.unarc.csv' DELIMITER ';' CSV ;

create table candidateentities(e int);
create index pkce on candidateentities(e);
cluster candidateentities using pkce;
copy candidateentities from 'tac.yagos.cands.tsv' DELIMITER ',' CSV ;

#Two edgepaths
   create table twoedgespaths as select e1.s,e2.o,e1.m as m1,e2.m as m2 from unarc as e1, unarc as e2 where e1.o=e2.s and e1.s!=e2.o and e1.s in (select e from candidateentities) and e2.o in (select e from candidateentities);
   
#Three edgepaths
   create table threeedgespaths as select e1.s, e3.o, e1.m as m1, e2.m as m2, e3.m as m3 from oneedgeonecandidateentity as e1, unarc as e2, oneedgeonecandidateentity as e3 where e1.o=e2.s and e1.s in (select e from candidateentities) and e2.o=e3.s and e3.o in (select e from candidateentities) and e1.s!=e3.o;
   
#Four edgepaths
   create table fouredgespaths as select e1.s, e4.o, e1.m as m1, e2.m as m2, e3.m as m3, e4.m as m4 from oneedgeonecandidateentity as e1, unarc as e2, unarc as e3, oneedgeonecandidateentity as e4 where e1.o=e2.s and e1.s in (select e from candidateentities) and e2.o=e3.s and e3.o=e4.s and e4.o in (select e from candidateentities) and e1.s!=e4.o;
   
################### BaseKB #########################
create table unarc(s int, o int, m float);
create index so on unarc (s,o);
create index os on unarc (o,s);
cluster unarc using so;
vacuum analyze;
copy unarc from 'basekb.unarc.csv' DELIMITER ';' CSV ;

create table candidateentities(e int);
create index pkce on candidateentities(e);
cluster candidateentities using pkce;
copy candidateentities from 'tac.basekb.cands.tsv' DELIMITER ',' CSV ;

#Two edgepaths
   create table twoedgespaths as select e1.s,e2.o,e1.m as m1,e2.m as m2 from unarc as e1, unarc as e2 where e1.o=e2.s and e1.s!=e2.o and e1.s in (select e from candidateentities) and e2.o in (select e from candidateentities);
   
#Three edgepaths
   create table threeedgespaths as select e1.s, e3.o, e1.m as m1, e2.m as m2, e3.m as m3 from oneedgeonecandidateentity as e1, unarc as e2, oneedgeonecandidateentity as e3 where e1.o=e2.s and e1.s in (select e from candidateentities) and e2.o=e3.s and e3.o in (select e from candidateentities) and e1.s!=e3.o;
   