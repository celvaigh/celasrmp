#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# -W ignore
import os,sys,time,math,gzip
import wikipedia
import numpy as np
import pandas as pd
from tqdm import tqdm
import concurrent.futures
from multiprocessing import Pool
import time
import requests
import gc
import tqdm


def triples2ids(filename):
	dic={}
	with open(filename) as f:
		data=f.read().split("\n")
	for row in tqdm.tqdm(data):
		try:
			_,s,r,o,_ = row.split("\t")
		except:print(row.split("\t"))
		if "rdf:type" in r:continue
		if "/" in s:s=s.rsplit("/")[-1]
		if "/" in o:o=o.rsplit("/")[-1]
		for c in ["<",">","ressource:"]:s,o=s.replace(c,""),o.replace(c,"")

		try: _ = dic[s]
		except:dic[s] = len(dic)
		try: _ = dic[o]
		except:dic[o] = len(dic)
	np.save("data/Yago.mid2numsdic.npy", dic)

def dic2csv(filename):
	dic=np.load(filename,encoding="latin1",allow_pickle=True).item()
	data = "\n".join([",".join([str(key[0]),str(key[1]),str(dic[key])]) for key in tqdm.tqdm(dic)])
	with open(filename.replace(".npy",".csv"), "w") as f:
		f.write(data)

	
#t-norme=ET et t-conorme= Ou

def compensatory(operator,l,p):
	t=operator(l,p,"n")
	s=operator(l,p,"c")
	a=t/(s+t)
	return (1-a)*t+a*s
def yeger(x,y,p,op):
	if op=="n":return max(0,1-math.pow(math.pow(1-x,p)+math.pow(1-y,p),1.0/p))
	elif op=="c":return min(1,math.pow(math.pow(x,p)+math.pow(y,p),1.0/p))
	
def yeger2(l,p,op):
	res=l[0]
	for i in range(1,len(l)):res=yeger(l[i],res,p,op)
	return res
	
def hamacher(x,y,p,op):
	if op=="n":return x*y/(p+(1-p)*(x+y-x*y))
	elif op=="c":return (x+y-x*y-(1-p)*x*y)/(1-(1-p)*x*y)
	
def hamacher2(l,p,op):
	res=l[0]
	for i in range(1,len(l)):res=hamacher(l[i],res,p,op)
	return res
	
def cheikh(x,y,p,op):
	if op=="n":
		if x>=p:return y
		elif y>=p:return x
		else:return 0
	elif op=="c":
		if x<=1-p:return y
		elif y<=1-p:return x
		else:return 1
def cheikh2(l,p,op):
	res=l[0]
	for i in range(1,len(l)):res=cheikh(l[i],res,p,op)
	return res
	
def weber(x,y,p,op):
	if op=="n":return max((x+y-1+p*x*y)/(1.0+float(p)),0)
	elif op=="c":min(x+y+p*x*y/(1.0+float(p)),1)
def weber2(l,p,op):
	res=l[0]
	for i in range(1,len(l)):res=weber(l[i],res,p,op)
	return res
	
	
def drastic(x,y,op):
	if op=="n":
		if x==1:return y
		elif y==1:return x
		else:return 0
	elif op=="c":
		if x==0:return y
		elif y==0:return x
		else:return 1
def drastic2(l,op):
	res=l[0]
	for i in range(1,len(l)):res=drastic(l[i],res,op)
	return res
	
	
def einstein(x,y,op):
	if op=="n":return x*y/(1+(1-x)*(1-y))
	elif op=="c":return (x+y)/(1+x*y)
	
def einstein2(l,op):
	res=l[0]
	for i in range(1,len(l)):res=einstein(l[i],res,op)
	return res
	
def OWAlike(l,a,op):
	n=len(l)
	l.sort(reverse=True)
	if op=="n":weights=[float((1-a))/n]*(n-1)+[a+float((1-a))/n]
	elif op=="c":weights=[a+float((1-a))/n]+[float((1-a))/n]*(n-1)
	return sum([l[i]*weights[i] for i in range(n)])

def BADD_OWA(l,a,op):
	if op=="n":
		denominator=sum([(1-i)**a for i in l])
		numerator= sum([i*(1-i)**a for i in l])
	elif op=="c":
		denominator=sum([i**a for i in l])
		numerator= sum([i**(a+1) for i in l])
	return numerator/denominator

def fuzion(filename,agg="yeger",p=1):
	
	if "bz2" in filename:compression="bz2"
	else:compression="infer"
	df=pd.read_csv(filename,header=None,chunksize=10000000,compression=compression)
	
	dic={}
	print("building dic with agg="+agg+" and p="+str(p))
	for df_chunk in tqdm(df):
		s=list(df_chunk[0])
		o=list(df_chunk[1])
		wsrm=[list(df_chunk[i]) for i in range(2,len(df_chunk.columns))]
		for i in range(len(s)):
			weights=[wsrmi[i] for wsrmi in wsrm]
			if agg=="yeger":m=yeger2(weights,p,"n")#t-norm =ET
			elif agg=="hamacher":m=hamacher2(weights,p,"n")
			elif agg=="einstein":m=einstein2(weights,"n")
			elif agg=="minmax":m=min(weights)
			elif agg=="owalike":m=OWAlike(weights,p,"n")
			elif agg=="badd":m=BADD_OWA(weights,p,"n")
			elif agg=="cheikh":m=cheikh2(weights,p,"n")
			elif agg=="drastic":m=drastic2(weights,"n")
			elif agg=="compensatory":m=compensatory(hamacher2,weights,p)
			else:print("operator ",agg," not defined excited ***");exit()
			try:
				if agg=="yeger":dic[(s[i],o[i])]=yeger(dic[(s[i],o[i])],m,p,"c")
				elif agg=="hamacher":dic[(s[i],o[i])]=hamacher(dic[(s[i],o[i])],m,p,"c")
				elif agg=="einstein":dic[(s[i],o[i])]=einstein(dic[(s[i],o[i])],m,"c")
				elif agg=="minmax":dic[(s[i],o[i])]=max(dic[(s[i],o[i])],m)
				elif agg=="owalike":dic[(s[i],o[i])]=OWAlike([dic[(s[i],o[i])],m],p,"c")
				elif agg=="badd":dic[(s[i],o[i])]=BADD_OWA([dic[(s[i],o[i])],m],p,"c")
				elif agg=="cheikh":dic[(s[i],o[i])]=cheikh2([dic[(s[i],o[i])],m],p,"c")
				elif agg=="drastic":m=drastic(dic[(s[i],o[i])],m,"c")
				elif agg=="compensatory":dic[(s[i],o[i])]=compensatory(hamacher2,[dic[(s[i],o[i])],m],p)
				else:print("operator ",agg," not defined excited ***");exit()
			except:dic[(s[i],o[i])]=m
	r,b=os.path.splitext(filename)
	if p==None:output=r+"."+agg+".fuzion.npy"	
	else:output=r+"."+agg+"."+str(p)+".fuzion.npy"	
	np.save(output, dic)


def fuzzy(dirname,paths=["oneedgepaths.csv","twoedgespaths.csv","threeedgespaths.csv"]):
	dfs=[]
	agg="hamacher";p=0
	pnames=["one","two","three","four","five"]
	for filename in paths:
		if "bz2" in filename:compression="bz2"
		else:compression="infer"
		dfs+=[pd.read_csv(dirname+filename,header=None,chunksize=10000000,compression=compression)]
	dic={}
	print("building dic with p="+str(p))
	for df in dfs:
		for df_chunk in tqdm(df):
			s=list(df_chunk[0])
			o=list(df_chunk[1])
			wsrm=[list(df_chunk[i]) for i in range(2,len(df_chunk.columns))]
			for i in range(len(s)):
				weights=[wsrmi[i] for wsrmi in wsrm]
				if agg=="yeger":m=yeger2(weights,p,"n")#t-norm =ET
				elif agg=="hamacher":m=hamacher2(weights,p,"n")
				else:print("operator ",agg," not defined excited ***");exit()
				try:
						if agg=="yeger":dic[(s[i],o[i])]=yeger(dic[(s[i],o[i])],m,p,"c")
						elif agg=="hamacher":dic[(s[i],o[i])]=hamacher(dic[(s[i],o[i])],m,p,"c")
						else:print("operator ",agg," not defined excited ***");exit()
				except:dic[(s[i],o[i])]=m
	print("saving and plotting final dic ...")
	output=dirname+str(pnames[len(paths)-1])+"."+agg+"."+str(p)+".fuzzy.npy"		
	np.save(output, dic)
	#dicDistrib(output)
	
def social_proximity1(dirname,paths=["oneedgepaths.csv","twoedgespaths.csv","threeedgespaths.csv"],y=1.5):
	dfs=[]
	agg="hamacher";p=0
	pnames=["one","two","three","four","five"]
	for filename in paths:
		if "bz2" in filename:compression="bz2"
		else:compression="infer"
		dfs+=[pd.read_csv(dirname+filename,header=None,chunksize=10000000,compression=compression)]
	dic={}
	print("building dic with alambda="+str(y))
	for df in dfs:
		for df_chunk in tqdm(df):
			s=list(df_chunk[0])
			o=list(df_chunk[1])
			wsrm=[list(df_chunk[i]) for i in range(2,len(df_chunk.columns))]
			for i in range(len(s)):
				weights=[wsrmi[i] for wsrmi in wsrm]
				if agg=="yeger":m=yeger2(weights,p,"n")#t-norm =ET
				elif agg=="hamacher":m=hamacher2(weights,p,"n")
				elif agg=="einstein":m=einstein2(weights,"n")
				elif agg=="minmax":m=min(weights)
				elif agg=="owalike":m=OWAlike(weights,p,"n")
				elif agg=="badd":m=BADD_OWA(weights,p,"n")
				elif agg=="cheikh":m=cheikh2(weights,p,"n")
				elif agg=="drastic":m=drastic2(weights,"n")
				elif agg=="compensatory":m=compensatory(hamacher2,weights,p)
				else:print("operator ",agg," not defined excited ***");exit()
				try:dic[(s[i],o[i])]+=m/(y**(len(weights)-2))
				except:dic[(s[i],o[i])]=m/(y**(len(weights)-2))
	print("normalizing dic ...")
	if y>1:
		Cy=(y-1)/y
		for i in dic:dic[i]=Cy*dic[i]
	print("saving and plotting final dic ...")
	output=dirname+str(pnames[len(paths)-1])+"."+agg+"."+str(p)+".prox1."+str(y)+".npy"		
	np.save(output, dic)
	#dicDistrib(output)
	
def social_proximity2(dirname,paths=["oneedgepaths.csv","twoedgespaths.csv","threeedgespaths.csv"],coefs=[1,0,0,0,0]):
	dfs=[]
	pnames=["one","two","three","four","five"]
	agg="hamacher";p=0
	for filename in paths:
		if "bz2" in filename:compression="bz2"
		else:compression="infer"
		dfs+=[pd.read_csv(dirname+filename,header=None,chunksize=10000000,compression=compression)]
	dic={}
	print("building dic with coefs="+str(coefs[len(paths)-1]))
	pp=0
	for df in dfs:
		for df_chunk in tqdm(df):
			s=list(df_chunk[0])
			o=list(df_chunk[1])
			wsrm=[list(df_chunk[i]) for i in range(2,len(df_chunk.columns))]
			for i in range(len(s)):
				weights=[wsrmi[i] for wsrmi in wsrm]
				if agg=="yeger":m=yeger2(weights,p,"n")#t-norm =ET
				elif agg=="hamacher":m=hamacher2(weights,p,"n")
				elif agg=="einstein":m=einstein2(weights,"n")
				elif agg=="minmax":m=min(weights)
				elif agg=="owalike":m=OWAlike(weights,p,"n")
				elif agg=="badd":m=BADD_OWA(weights,p,"n")
				elif agg=="cheikh":m=cheikh2(weights,p,"n")
				elif agg=="drastic":m=drastic2(weights,"n")
				elif agg=="compensatory":m=compensatory(hamacher2,weights,p)
				else:print("operator ",agg," not defined excited ***");exit()
				try:dic[(s[i],o[i])]+=m*coefs[pp]
				except:dic[(s[i],o[i])]=m*coefs[pp]
		pp+=1
	print("saving dic ...")
	output=dirname+str(pnames[len(paths)-1])+"."+agg+"."+str(p)+".prox2."+str(coefs[len(paths)-1])+".npy"	
	np.save(output, dic)
	#dicDistrib(output)

if __name__ == "__main__":
	kb = "Yago"
	for path in ["twoedgespaths.csv","threeedgespaths.csv"]:
		fuzion("data/{}.{}".format(kb, path))
	fuzzy("data/")
	# social_proximity1("data/")
	# set the values for coefs with greedy search
	# social_proximity1("data/")
	