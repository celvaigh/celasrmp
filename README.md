# CELASRMP
A Novel Path-based Entity Relatedness Measure for Efficient Collective Entity Linking

## Local scores
```python
 Cosine similarity : Skip-gram
 
 Popularity : Cross-Wiki + Wikipedia
 ```
 
## Relatedness
```python
 WSRM
 
 ASRMP_m
 ```
## Linking strategy
```python
 We adopt a supervised approach, where a binary classifier is trained to predict whether a mention and a candidate entity are related (1) or not (0).
 The features used are, the some and the three greatest values of relatedness scores within one document.
```
## Datasets

- [TAC EDL 2016-2017](https://www.ldc.upenn.edu/)
- [N3-Collection](https://github.com/dice-group/n3-collection/)
   - N3/Reuters-128.ttl
   - N3/RSS-500.ttl
- [AIDA](http://resources.mpi-inf.mpg.de/yago-naga/aida/download/aida-yago2-dataset.zip) : Follow the instructions in the README
   - AIDA-train
   - AIDA-testa
   - AIDA-testb
 
 
### Setup

1. Download datasets and the target KB. For Yago use this [link](https://www.mpi-inf.mpg.de/departments/databases-and-information-systems/research/yago-naga/yago/downloads) and choice YagoFacts.

2. Clone the repository: 
    
    `git clone https://gitlab.inria.fr/celvaigh/celasrmp.git`

3. Install dependencies:
    - Python
    - libs in the requirements file 
    ```python
       pip install -r requirements.txt
    ```
4. Compute WSRM using its definition in 
    
    ```python
        python Metrics.py
    ```
5. Compute ASRMP_m using the sql queries in `ASRMP.sql`
6. Compute the word2vec embedding using [wikipedia dump](https://ftp.acc.umu.se/mirror/wikimedia.org/dumps/enwiki/20200801/enwiki-20200801-pages-articles.xml.bz2), or use [our pretrained word2vec](https://drive.google.com/file/d/1GZjGNmWNM44_8ggoODQ5RUhrQ0Gu7lQZ/view?usp=sharing).
7. Compute commonness using the previous Wikipedia dump
8. Download [cross-wiki](https://drive.google.com/file/d/0Bz-t37BfgoTuSEtXOTI1SEF3VnM/view)
9. Download [our mappings wikipedia to basekb and wikipedia to yago](https://drive.google.com/drive/folders/1Es45rJvLTzIHHdLp0-6hlT959E3mzl6x?usp=sharing)
10. Compute features using 

    ```python
        python linking.py
    ```
11. Test the model using classification technique (REG/SVM/MLP)
